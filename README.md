# git-sc-common

- `git-sc-common`: scala commons library using tapir 1.7
- `git-sc-common-tapir`: scala commons library using tapir 1.2.3

("git" means updates are triggered manually using git commands)

---
new line inserted
---

### mir-sc-common -> mir-sc-common-tapir

(based on: https://forum.gitlab.com/t/refreshing-a-fork/32469/2)

In the `mir-sc-common-tapir` project from the command line:

Add a new upstream:
```bash
git remote add upstream https://gitlab.com/gustavo.muniz1/git-sc-common.git
``` 

To get updates from `git-sc-common`:

```bash
git checkout main
git fetch upstream
git pull upstream main
```

Also to keep up-to-date the `main` branch in `git-sc-common-tapir`:

```bash
git push origin main
```

Pros:
- no need to change settings in projects
- no need to create a PAT (personal access token)
- **in case of conflicts when updating, they can be fixed as usual in git** (mirroring once the process fail, can't be fixed)

Cons:
- must be triggered manually (this is good and bad at the same time...)

### mir-sc-common <- mir-sc-common-tapir

Usually we don't need it, except when:

- all services are migrated to the latest tapir version
- we need to move all changes to `scala-commons`

## Cases

### Both `master` branches are in sync in both projects

Then new updates in `master` in `git-sc-common`.
Project `git-sc-common-tapir` was updated successfully.

### New commits in `master` branch in `git-sc-common-tapir`

Update failed but conflicts were resolved as usual in git.

## Possible workflow

- everytime `scala-commons` gets updated, `scala-commons-tapir` is also updated
- in `scala-commons-tapir` we work in a feature branch without commiting in `master` (*)
- we release versions of `scala-commons-tapir` from the feature branch, as is now
- migrated service use snapshot version
- when migration is finished a new MR(**) is created:
  - from `scala-commons-tapir` to `scala-commons`
  - it can be reviewed, commented, rejected, etc.
  - when approved `scala-commons-tapir` can be discarded/closed/archived

*: this is the easy way. Otherwise we can commit in master but then we may have to solve conflicts when pulling latest changes from `scala-commons`.
**: only if we forked `scala-commons-tapir`

